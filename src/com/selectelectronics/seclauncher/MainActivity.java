package com.selectelectronics.seclauncher;

import java.io.DataOutputStream;

import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;

public class MainActivity extends Activity
{
	protected Dialog	mSplashDialog;

	@Override
	// *************************************************
	// MAIN STARTUP
	// *************************************************
	protected void onCreate(Bundle savedInstanceState)
	{

		Log.d("SECLauncher", "OnCreate");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	// *************************************************
	// ON RESUME EVENT HANDLER
	// *************************************************

	@SuppressWarnings("deprecation")
	@Override
	public void onResume()
	{
		Log.d("SECLauncher", "OnResume");

		hideSystemUI();

		// Turn off lock
		KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
		KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
		lock.disableKeyguard();

		super.onResume();
		showSplashScreen();
	}

	// *************************************************
	// DISPLAY SPLASH SCREEN DIALOG
	// *************************************************

	protected void showSplashScreen()
	{
		Log.d("SECLauncher", "Show Splash Screen");

		mSplashDialog = new Dialog(this);// , R.style.SplashScreen);
		mSplashDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		mSplashDialog.setContentView(getLayoutInflater().inflate(R.layout.splashscreen, null));
		mSplashDialog.setCancelable(false);
		mSplashDialog.show();

		// *************************************************
		// KEY LISTENER DURING SPLASH SCREEN
		// *************************************************

		mSplashDialog.setOnKeyListener(new Dialog.OnKeyListener()
		{

			@Override
			public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event)
			{

				Log.d("SECLauncher", "Keycode Listener");

				if (keyCode == KeyEvent.KEYCODE_ESCAPE)
				{
					Log.d("SECLauncher", "ESC Key hit");
					removeSplashScreen();
					showSystemUI();

					Log.d("SECLauncher", "Clear default launcher");
					getPackageManager().clearPackagePreferredActivities(getPackageName());

					Log.d("SECLauncher", "Kill SECLauncher");
					finish();
				}
				return true;
			}
		});

		// Set Runnable to remove splash screen after 7 seconds

		final Handler handler1 = new Handler();
		handler1.postDelayed(new Runnable()
		{

			@Override
			public void run()
			{
				removeSplashScreen();
				showSystemUI();

				Log.d("SECLauncher", "Load ChefTab");
				// PackageManager pm = getPackageManager();
				// Intent intent = pm.getLaunchIntentForPackage("cheftab.apk");
				// startActivity(intent);

				Log.d("SECLauncher", "Kill SECLauncher");
				finish();
				// android.os.Process.killProcess(android.os.Process.myPid());
			}
		}, 7000);

		// OK while we are waiting lets look for keystrokes

		Log.d("SECLauncher", "Waiting...");
	}

	// *************************************************
	// REMOVE THE SPLASH SCREEN DIALOG
	// *************************************************

	protected void removeSplashScreen()
	{
		Log.d("SECLauncher", "Remove Splash Screen");

		if (mSplashDialog != null)
		{
			mSplashDialog.dismiss();
			mSplashDialog = null;
		}
	}

	// *************************************************
	// SHOW SYSTEMUI
	// *************************************************

	public static boolean showSystemUI()
	{
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		{
			Log.d("SECLauncher", "ShowUI Skipped");
			return true;
		}

		Log.d("SECLauncher", "ShowUI Started");

		try
		{
			Process mSuProcess;
			mSuProcess = Runtime.getRuntime().exec("sh");

			DataOutputStream mSuDataOutputStream = new DataOutputStream(mSuProcess.getOutputStream());

			mSuDataOutputStream.writeBytes("am startservice --user 0 -n com.android.systemui/.SystemUIService\n");
			// mSuProcess.destroy();
			Log.d("SECLauncher", "ShowUI Complete");
			return true;
		}
		catch (Exception e)
		{
			Log.d("SECLauncher", "ShowUI " + e.toString());
			return false;
		}
	}

	// *************************************************
	// HIDE SYSTEMUI
	// *************************************************

	public static boolean hideSystemUI()
	{
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		{
			Log.d("SECLauncher", "HideUI Failed OS version less than ICS");
			return true;
		}

		Log.d("SECLauncher", "HideUI Started");

		try
		{

			Process mSuProcess;
			mSuProcess = Runtime.getRuntime().exec("sh");

			DataOutputStream mSuDataOutputStream = new DataOutputStream(mSuProcess.getOutputStream());

			mSuDataOutputStream.writeBytes("su -c service call activity 42 s16 com.android.systemui\n");
			Log.d("SECLauncher", "HideUI Complete");
			// mSuProcess.destroy();
			return true;
		}
		catch (Exception e)
		{
			Log.d("SECLauncher", "hideUI " + e.toString());
			return false;
		}
	}

}